<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * Hook Woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>

<?php if ( version_compare( WOOCOMMERCE_VERSION, '3.4' ) >= 0 ) { ?>
    <div id="product-<?php the_ID(); ?>" <?php wc_product_class(); ?>>
<?php }
else { ?>
    <div id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php } ?>
	
	<?php
		/**
		 * Hook: woocommerce_before_single_product_summary.
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );
	?>

	<div class="summary entry-summary">
		<div class="clearfix">
			<?php
				/**
				 * woocommerce_single_product_summary hook.
				 *
				 * @hooked woocommerce_template_single_title - 5
				 * @hooked woocommerce_template_single_rating - 10
				 * @hooked woocommerce_template_single_price - 10
				 * @hooked woocommerce_template_single_excerpt - 20
				 * @hooked woocommerce_template_single_add_to_cart - 30
				 * @hooked woocommerce_template_single_meta - 40
				 * @hooked woocommerce_template_single_sharing - 50
				 * @hooked WC_Structured_Data::generate_product_data() - 60
				 */
				do_action( 'woocommerce_single_product_summary' );
			?>
		</div><!-- .clearfix -->
	</div><!-- .summary -->
	
	<?php
		/**
		 * Hook: woocommerce_after_single_product_summary.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );
	?>
</div>
 <h5 class="nomAmbiente">AMBIENTE</h5>
 <div class="container-fluid">
  	<div class="row align-items-center">
  		<?php 
  			$image1 = get_field('ambiente');
  		?>
    	<div class="col-md-6">
      		<img src="<?php echo $image1; ?>" alt="" width="100%">
    	</div>
    	<div class="col-md-6 txtAmbiente">
    		<p>
	   		<?php
	      		$variableAmbiente1 = get_field('texto_ambiente_1');
	      		echo $variableAmbiente1; 
	     	?>
	     </p>
    	</div>
  	</div>
 	<div class="row align-items-center">
    	<div class="col-md-6 txtAmbiente">
    		<p>
      		<?php
      			$variableAmbiente2 = get_field('texto_ambiente2');
      			echo $variableAmbiente2; 
      		?>
      	</p>
    	</div>
    	<?php
			$image2 = get_field('ambiente2');
    	?>
    	<div class="col-md-6">
    		<img src="<?php echo $image2; ?>" alt="" width="100%">
    		
   		</div>
   	</div>
 </div>
<div class="titularAcord" data-toggle="collapse" data-target="#demo">  MATERIALES Y DIMENSIONES</div>

<div id="demo" class="collapse">
 			<strong>Estructura:</strong>
           	<?php 
           		$variableEstructura = get_field('estructura');
           		echo $variableEstructura; 
           	?> <br />
  			<strong>Materiales: </strong> 
  			<?php 
  				$variableMateriales = get_field('materiales');
      			echo $variableMateriales; 
			?> <br />
    		<strong>Planos</strong><br />
    		<?php 
    			$image = get_field('imagen_plano');
    		?>
			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
</div>
<div class="titularAcord" data-toggle="collapse" data-target="#demo1"> VARIANTES</div>
<div id="demo1" class="collapse">
 			<?php 
				$images3 = get_field('variantes');
				if( $images3 ): ?>
				    <div class="container-fluid">
				    	<div class="row align-items-center">
				    		<div class="col-sm-6">
								<h3> Esta silla esta disponible en estos colores: </h3>
				    		</div>
				        <?php foreach( $images3 as $image ): ?>
				            <div class="col-sm-3">
				            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				            </div>
				        <?php endforeach; ?>
				    	</div>
				    </div>
				<?php endif; ?>


</div>



<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<?php do_action( 'woocommerce_after_single_product' ); ?>
